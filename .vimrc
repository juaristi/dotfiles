set nocompatible
set number
filetype off

set rtp+=~/.vim/bundle/Vundle.vim

" Plugins
call vundle#begin()
Plugin 'ervandew/supertab'
Plugin 'altercation/vim-colors-solarized'
Plugin 'nanotech/jellybeans.vim'
Plugin 'ciaranm/securemodelines'
Plugin 'scrooloose/nerdtree'
Plugin 'fholgado/minibufexpl.vim'
Plugin 'majutsushi/tagbar'
Plugin 'joshdick/onedark.vim'
Plugin 'tpope/vim-commentary'
Plugin 'davidhalter/jedi-vim'
call vundle#end()

let g:solarized_termcolors=256
let g:miniBufExplCycleArround=1

syntax enable
set background=dark
" colorscheme solarized
" colorscheme jellybeans
colorscheme onedark

filetype plugin indent on
ru! ftplugin/man.vim

function! FindLocally(filename)
    if w:root_directory == ""
        echohl ErrorMsg
        echo "Could not find file. Root directory not given"
        echohl None
        return
    endif
    let s:root_directory = (glob(w:root_directory) == "" ? w:root_directory : glob(w:root_directory))
    let s:find_pattern = s:root_directory . '/include/*'
    let s:dstfile = findfile(a:filename, s:find_pattern)
    if s:dstfile == ""
        echohl ErrorMsg
        echomsg "Could not find file " . a:filename
        echohl None
        return
    endif

    echo "Opening file " . s:dstfile
    exe "e " . fnameescape(s:dstfile)
endfunction

function! OpenCHeaderFile(filename, local_only)
    let s:homedir = glob('~')

    let s:find_pattern = '.'
    if a:local_only == 0
        let s:find_pattern = s:find_pattern . ";" . s:homedir . ",/usr/include,/usr/local/include"
    endif

    let s:dstfile = findfile(a:filename, s:find_pattern)
    if s:dstfile == ""
        let s:dstfile = findfile(a:filename, "/usr/include/x86_64-linux-gnu")
        if s:dstfile == ""
            return FindLocally(a:filename)
        endif
    endif

    echo "Opening file " . s:dstfile
    exe "e " . fnameescape(s:dstfile)
endfunction

function! ParseCInclude()
    " Parse an #include directive on the current line
    let cincl = matchlist(getline(line('.')), '#[ ]*include \([<"]\)\?\([a-z0-9_\-\/]*\.h\)\?\([>"]\)\?')
    if len(cincl) > 0
        let s:local_only = (cincl[1] == '"' && cincl[3] == '"')
        return OpenCHeaderFile(cincl[2], s:local_only)
    endif
endfunction

function! ResetRoot()
    let w:root_directory = ""
endfunction

au VimEnter,WinNew * :call ResetRoot()

nmap <F3> :call ParseCInclude()<CR>
nmap <F4> :syntax sync fromstart<CR>
nmap t <C-]>
nmap T :call SearchTag()<CR>
nmap <C-h> <C-w>>
nmap <C-l> <C-w><
nmap <C-c>s :colorscheme solarized<CR>:set background=dark<CR>
nmap <C-c>d :colorscheme default<CR>:set background=dark<CR>
nmap <C-c>b :colorscheme darkblue<CR>:set background=dark<CR>
nmap <C-c>j :colorscheme jellybeans<CR>:set background=dark<CR>
nmap <C-c>o :colorscheme onedark<CR>
nmap <C-n> :NERDTreeToggle<CR>
nmap <C-d> :MBEbd<CR>
nmap <Tab> :MBEbn<CR>
nmap <S-Tab> :MBEbp<CR>
nmap <F8> :TagbarToggle<CR>

function! SetInd(ts, sw, sts, spaces)
    execute "set ts=" . a:ts . " sw=" . a:sw . " sts=" . a:sts
    if a:spaces
        execute "set si et"
    else
        execute "set nosi noet"
    endif
endfunction

" Start up NERDTree on opening a directory ('vim .')
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif

" Tell NERDTree to ignore some files
let NERDTreeIgnore=['\.o$', '\.lo$', '\.la$', '\.pyc$']

" Autocommands - set indentation for various file types
au BufNewFile,BufRead *.py :call SetInd(4,4,4,1)
au FileType python :call SetInd(4,4,4,1)

au BufNewFile,BufRead *.yml,*.yaml :call SetInd(2,2,2,1)
au FileType yml :call SetInd(2,2,2,1)

au BufNewFile,BufRead *.c,*.h :call SetInd(8,8,8,0)
au BufNewFile,BufRead *.c,*.h :setlocal foldmethod=syntax

au BufNewFile,BufRead *.vim,.vimrc :call SetInd(4,4,4,1)
au FileType vim :call SetInd(4,4,4,1)

au BufNewFile,BufRead *.html,*.htm :call SetInd(2,2,2,1)
au FileType html :call SetInd(2,2,2,1)

au BufNewFile,BufRead *.sh,.bashrc :call SetInd(4,4,4,1)
au FileType sh :call SetInd(4,4,4,1)

au BufNewFile,BufRead Dockerfile :call SetInd(4,4,4,1)
au FileType dockerfile :call SetInd(4,4,4,1)

" Folding
au FileType c :setlocal foldmethod=syntax
au FileType python :set foldmethod=indent  " Rudimentary folding for Python - works well in most of the cases
